#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(long long int p){
    bool prime = true;
    long long int mff, divisor;
    mff = (long long int)sqrt(p)+1;
    divisor = 3;
    while(divisor < mff && prime == true){
        if(p % divisor==0){
            prime = false;
        }
        divisor++;
    }
    return prime;
}

int main(){
    long long int n;
    int count = 1;
    n = 3; //n i s the number we check to see if it is prime.
    bool prime;
    cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
    cout<<"2 \t\t 1"<<endl;
        while (n < 4000){
            prime = isPrime(n);
            if (prime) {
                count++;
                cout<<n<<"\t"<<count<<"\t"<<prime<<endl;
            }
            n = n + 2;
        }

}

